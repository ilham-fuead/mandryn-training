# Mandryn Documentation

## 1. DBQuery (mysqli)

### Opening connection

```php
$DBQueryObj=new DBQuery($host, $username, $password, $database_name);
```

### Assigning SQL string

```php
$sql= <<<SQL
SELECT first_name,last_name
FROM friend
SQL;

$DBQueryObj->setSQL_Statement($sql);
```

### Returning/displaying assigned SQL string

```php
/* As input to other method or direct output (with echo) */
echo $DBQueryObj->getSqlString();
```

### Executing database query

```php
$DBQueryObj->runSQL_Query();
```

### Checking return recordset from executed query

```php
if($DBQueryObj->isHavingRecordRow()){

}
```

### Retrieving recordset from database query

#### i. Using **fetchRow** method

>"Suitable for medium to high volume result return recordset query."

```php
if($DBQueryObj->isHavingRecordRow()){
    while($row=$DBQueryObj->fetchRow()){
        /* Manipulating array $row here */    
    }
}
```

#### ii. Using **yieldRow** method

>"Very fast return query. Suitable for small set return recordset. Example for UI dropdown binding etc."

```php
foreach ($DBQueryObj->yieldRow() as $row){
    $rows[]=$row;
}
/* Manipulating array $rows here */  
```

#### iii. Using **getRowsInJSON** method

>"Returning result recordset in JSON string format. Suitable for AJAX, xmlHttpRequest & Promise type application."

```php
/* as input to other method or direct output (with echo) */
echo $DBQueryObj->getRowsInJSON();
```

## 2. DBQueryi (mysqli/PDO)

## 3. DBCommand

### i. Initializing DBCommand object. 

>"Need DBQuery object as parameter"

```php
$DBCmd=new DBCommand($DBQueryObj);
```

### ii. Insert Command 

```php
const TABLE_NAME='friend';

/* Set table to insert */
$DBCmd->setINSERTintoTable(TABLE_NAME);
```

Use **addINSERTcolumn** method to configure field for insertion.

This method expect *3 parameters* which are:

| Parameter | Type | Remarks |
| :---:     | ---  | ---     |
| field_name | string | Name match DB field name |
| field_value | mixed | Value for field |
| field_type | string | May use constant IFieldType::*  |

Parameter **field_type** can have any following value:

* IFieldType::_STRING_TYPE_
* IFieldType::_INTEGER_TYPE_
* IFieldType::_FLOAT_TYPE_ :clown:
* IFieldType::_DATETIME_TYPE_
* IFieldType::_UNKNOWN_

Example 1 : Composing fields to insert

```php
const FIELD1_NAME='first_name';
const FIELD1_VALUE='Ilham';
const FIELD1_TYPE= IFieldType::STRING_TYPE;

const FIELD2_NAME='last_name';
const FIELD2_VALUE='Fuead';
const FIELD2_TYPE= IFieldType::STRING_TYPE;

const FIELD3_NAME='date_of_birth';
const FIELD3_VALUE='1979-1-29';
const FIELD3_TYPE= IFieldType::DATETIME_TYPE;

$DBCmd->addINSERTcolumn(FIELD1_NAME, FIELD1_VALUE, FIELD1_TYPE);
$DBCmd->addINSERTcolumn(FIELD2_NAME, FIELD2_VALUE, FIELD2_TYPE);
$DBCmd->addINSERTcolumn(FIELD3_NAME, FIELD3_VALUE, FIELD3_TYPE);
```

Example 2 : Composing fields to insert

```php
$fields = [
    [
        'name' => 'first_name',
        'value' => 'Ilham',
        'type' =>IFieldType::STRING_TYPE
    ],
    [
        'name' => 'last_name',
        'value' => 'Fuead',
        'type' =>IFieldType::STRING_TYPE
    ],
    [
        'name' => 'date_of_birth',
        'value' => '1989-10-4',
        'type' =>IFieldType::DATETIME_TYPE
    ]
];

/* Populating addINSERTcolumn method */
foreach($fields as $field){
    $DBCmd->addINSERTcolumn($field['name'], $field['value'], $field['type']);
}
```

```php
/* Execute command */
$DBCmd->executeQueryCommand(); 

/* Check if command is successfull */
if($DBCmd->getExecutionStatus()===true){
    /* retrieving inserted id */
    $insertedId=$DBCmd->getInsertID();
    echo "Inserted ID: $insertedId";
}else{
    $error_no=$DBCmd->getErrno();
    $error_message=$DBCmd->getError();
    echo "$error_no: $error_message";
}
```

### ii. Update Command 

```php
const TABLE_NAME='friend';

/* Set table to insert */
$DBCmd->setUPDATEtoTable(TABLE_NAME);
```

Use **addUPDATEcolumn** method to configure field for updating.

This method expect *3 parameters* which are:

* _field_name_
* _field_value_
* _field_type_

Parameter field_type can have any following value:

* IFieldType::_STRING_TYPE_
* IFieldType::_INTEGER_TYPE_
* IFieldType::_FLOAT_TYPE_ :clown:
* IFieldType::_DATETIME_TYPE_
* IFieldType::_UNKNOWN_

Example 1 : Composing fields to update

```php
const FIELD1_NAME='first_name';
const FIELD1_VALUE='Ilham';
const FIELD1_TYPE= IFieldType::STRING_TYPE;

const FIELD2_NAME='e_mail';
const FIELD2_VALUE='ilham@mandryn.net';
const FIELD2_TYPE= IFieldType::STRING_TYPE;

$DBCmd->addUPDATEcolumn(FIELD1_NAME, FIELD1_VALUE, FIELD1_TYPE);
$DBCmd->addUPDATEcolumn(FIELD2_NAME, FIELD2_VALUE, FIELD2_TYPE);
```

Use **addConditionStatement** method to configure condition field for updating.

This method expect **4** mandatory & **2** optional parameters which are:

* _field_name_
* _field_value_
* _field_type_
* _field_condition_operator_
* _field_comparison_type_ [optional]
* _field_wildcard_position_ [optional]

Parameter field_type can have any following value:

* IFieldType::_STRING_TYPE_ 
* IFieldType::_INTEGER_TYPE_
* IFieldType::_FLOAT_TYPE_ :clown:
* IFieldType::_DATETIME_TYPE_
* IFieldType::_UNKNOWN_

Parameter field_condition_operator can have any following value:

* IConditionOperator::_NONE_ (default)
* IConditionOperator::_AND_OPERATOR_
* IConditionOperator::_OR_OPERATOR_ 

Parameter field_comparison_type can have any following value:

* IComparisonType::_EQUAL_TO_ (default)
* IComparisonType::_NOT_EQUAL_TO_
* IComparisonType::_GREATER_THAN_
* IComparisonType::_LESS_THAN_
* IComparisonType::_LESS_THAN_OR_EQUAL_TO_
* IComparisonType::_STRING_LIKE_

Parameter field_wildcard_position can have any following value:

* IWildcardPosition::_NONE_ (default)
* IWildcardPosition::_PREPEND_
* IWildcardPosition::_APPEND_
* IWildcardPosition::_ENCLOSE_

Example 1 : Composing condition fields to update

```php
const FIELD1_NAME='first_name';
const FIELD1_VALUE='Ilham';
const FIELD1_TYPE= IFieldType::STRING_TYPE;
const FIELD1_CONDITION_OPERATOR= IConditionOperator::NONE;
const FIELD1_COOMPARISON_TYPE= IComparisonType::STRING_LIKE;
const FIELD1_WILDCARD_POS= IWildcardPosition::ENCLOSE;

const FIELD2_NAME='id';
const FIELD2_VALUE=5;
const FIELD2_TYPE= IFieldType::INTEGER_TYPE;
const FIELD2_CONDITION_OPERATOR= IConditionOperator::OR_OPERATOR;

/* 1st condition must have condition operator set to NONE since no other */
/* condition exist previously  to combine */
$DBCmd->addConditionStatement(FIELD1_NAME, FIELD1_VALUE, FIELD1_TYPE, FIELD1_CONDITION_OPERATOR, FIELD1_COOMPARISON_TYPE, FIELD1_WILDCARD_POS);

/* 2nd condition will have dafault value for comparion type(EQUAL_TO) and */
/* wildcard position(NONE) */
$DBCmd->addConditionStatement(FIELD2_NAME, FIELD2_VALUE, FIELD2_TYPE, FIELD2_CONDITION_OPERATOR);
```

```php
/* Execute command */
$DBCmd->executeQueryCommand(); 

/* Check if command is successfull */
if($DBCmd->getExecutionStatus()===true){
    /* Retrieving affected row count on update */
    $rowCount=$DBCmd->getAffectedRowCount();
    echo "Successfully! $rowCount records updated!";
}else{
    $error_no=$DBCmd->getErrno();
    $error_message=$DBCmd->getError();
    echo "$error_no: $error_message";
}
```
## 4. UniversalPaging
## 5. MagicObject
![Change array key via MagicObject](/images/Noname1.jpg)
## 6. MagicInput
## 7. Login


<?php

require_once '../vendor/autoload.php';
require_once '../config/db_connection.php';

$DBQueryObj = new DBQuery($host, $username, $password, $database_name);

/* Initializing DBCommand object. Need DBQuery object as parameter */
$DBCmd = new DBCommand($DBQueryObj);

/* Insert Command */
const TABLE_NAME = 'friend';

/* Set table to insert */
$DBCmd->setINSERTintoTable(TABLE_NAME);

/* Set field to insert 
const FIELD1_NAME = 'first_name';
const FIELD1_VALUE = 'Sutan';
const FIELD1_TYPE = IFieldType::STRING_TYPE;

const FIELD2_NAME = 'last_name';
const FIELD2_VALUE = 'Ilham';
const FIELD2_TYPE = IFieldType::STRING_TYPE;

const FIELD3_NAME = 'date_of_birth';
const FIELD3_VALUE = '1979-1-29';
const FIELD3_TYPE = IFieldType::DATETIME_TYPE;

$DBCmd->addINSERTcolumn(FIELD1_NAME, FIELD1_VALUE, FIELD1_TYPE);
$DBCmd->addINSERTcolumn(FIELD2_NAME, FIELD2_VALUE, FIELD2_TYPE);
$DBCmd->addINSERTcolumn(FIELD3_NAME, FIELD3_VALUE, FIELD3_TYPE);
*/
$fields = [
    [
        'name' => 'first_name',
        'value' => 'Sutan',
        'type' =>IFieldType::STRING_TYPE
    ],
    [
        'name' => 'last_name',
        'value' => 'Magek',
        'type' =>IFieldType::STRING_TYPE
    ],
    [
        'name' => 'date_of_birth',
        'value' => '1989-10-4',
        'type' =>IFieldType::DATETIME_TYPE
    ]
];

/* Populating addINSERTcolumn method */
foreach($fields as $field){
    $DBCmd->addINSERTcolumn($field['name'], $field['value'], $field['type']);
}

/* Execute command */
$DBCmd->executeQueryCommand();

/* Check if command is successfull */
if ($DBCmd->getExecutionStatus() === true) {
    /* retrieving inserted id */
    $insertedId = $DBCmd->getInsertID();
    echo "Inserted ID: $insertedId";
} else {
    $error_no = $DBCmd->getErrno();
    $error_message = $DBCmd->getError();
    echo "$error_no: $error_message";
}




